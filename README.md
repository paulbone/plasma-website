# Plasma Language Website
## *a new programming language*

This is the source for the plasma website.
For a general overview of plasma, please visit
[http://plasmalang.org/](http://plasmalang.org/)

Source code hosting has now moved to
[github](https://github.com/PaulBone/plasma).

